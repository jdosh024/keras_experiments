from collections import Counter

from keras import optimizers

import summ_preprocess
import _pickle as pickle
import json
#import keras
import keras
import pandas as pd
from keras.preprocessing import text,sequence
from keras.preprocessing.text import Tokenizer
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout, RepeatVector
from keras.layers.wrappers import TimeDistributed
from keras.layers.recurrent import LSTM
from keras.layers.embeddings import Embedding
from keras.regularizers import l2
from sklearn.model_selection import train_test_split
import data_preprocessing as dp

n_epoch = 30,
hidden_dim = 512,
emb_dim = 256,
max_enc_steps = 500,
max_dec_steps = 30,
vocab_size = 5000,
batch_size_summ = 100,
batch_size_ent = 5,
rand_unif_init_mag = 0.02,
trunc_norm_init_std = 1e-4,
max_grad_norm = 2.0,
lrate = 0.15,
optimizer = 'Adam',
use_dropout = True,
dropout = 0.25
verbose = False,
loss = 'categorical_crossentropy'



# x - article, y - abstract
def main():
    path = 'raw_data/signalmedia-1m.jsonl/sample.jsonl'
    article, abstract = getRequiredData(path)
    print(abstract)
    summ_preprocess.build_pickle(article=article,abstract=abstract)

    with open('processed_data/tokens.pkl','rb') as f:
        desc, heads = pickle.load(f)

    print('desc...')
    print(desc)
    print('head....')
    print(heads[0])
    path = '../GLOVE_DIR/'
    word_embeddings, embeddings_index = dp.process_glove(path)
    # create the tokenizer
    # t = Tokenizer()
    # # fit the tokenizer on the documents
    # t.fit_on_texts(abstract)
    # t.fit_on_texts(article)
    # integer encode abstract


    x_train = sequence.pad_sequences(desc, maxlen=None, dtype='float32',padding='pre', value=0.)
    y_train = sequence.pad_sequences(heads, maxlen=None, dtype='float32',padding='pre', value=0.)
    input_shape = x_train.shape
    output_shape = y_train.shape
    input_time_seq = input_shape[0]
    input_dim = input_shape[1]
    output_time_seq = output_shape[0]
    output_dim = output_shape[1]

    # print(input_shape[0])
    # print(input_shape[1])
    # print(output_shape)

    x_train,y_train,x_test,y_test = train_test_split(x_train,y_train)

    print(len(x_train))
    print(len(y_train))
    print(len(x_test))
    print(len(y_test))

    np.random.seed(7)  # for reproducibility

    # Reshape training data for Keras LSTM model
    # The training data needs to be (batchIndex, timeStepIndex, dimentionIndex)
    # Single batch, 9 time steps, 11 dimentions
    #
    # x_train = x_train.reshape((100,input_shape[0],input_shape[1] ))
    # y_train = y_train.reshape((100, output_shape[0],output_shape[1]))
    # x_train = x_train.reshape((100,input_time_seq,-1))
    # y_train= y_train.reshape((100,output_time_seq,-1))


    #encoder
    encoder = build_encoder_model(embedding=word_embeddings,shape=input_shape)
    adam= optimizers.Adam(lr=lrate)
    encoder.compile(loss=loss, optimizer=adam)
    encoder.save_weights('output/embeddings.h5py',overwrite=True)

    encoder.train_on_batch(x_train,y_train)
    scores = encoder.evaluate(x_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1] * 100))
    #
    # # initiate training
    # for i in range(30):
    #     print("epoch... " +str(i) )
    #
    #     #x_train_f,x_test,y_train_f,y_test = train_test_split(x_train,y_train,test_size=0.2,random_state=1)
    #     encoder.train_on_batch(x_train, y_train)
    #     #encoder.evaluate(x_test,y_test)
    #     keras.callbacks.ModelCheckpoint('model.p', monitor='val_loss', verbose=0, save_best_only=False,
    #                                     save_weights_only=False, mode='auto', period=1)
    #     encoder.save("text_summ.h5py", overwrite=False,include_optimizer=True)





    #decoder

    # with open('output/embeddings.pkl','rb') as fp:
    #     embeddings = pickle.load(fp.read())
    # decoder = build_model(embeddings)
    #decoder.

# def datarray_to_3D(data, dim1=3):
#     nr, nc = data.shape
#     data = data.reshape(nr, int(nc/dim1), dim1)
#     return np.rollaxis(data, 2, 1)

def getRequiredData(file):
    df = dp.preprocess(file)
    abstract = df['title']
    article = df['content']
    return article,abstract

def get_vocab(word):
    vocab = []
    listVocab = []
    for txt in word:
        vocab.append(txt.split())

    for sent in vocab:
        for word in sent:
            listVocab.append(word)

    vocabcount = Counter(listVocab)
    print(vocabcount)
    return listVocab, vocabcount, vocab  # format is --> [[the, happy,go][the, name, is]] --> each sentencewise

# def get_glove_embedding(path):
#     glove_weights = get_glove_weights(path)
#     word_embeddings = get_glove_matrix(glove_weights)
#     return word_embeddings

# def get_glove_weights(path):
#     words = pd.read_table(path, sep=" ", index_col=0, header=None, quoting=csv.QUOTE_NONE)
#     return words
#
# def get_glove_matrix(words):
#   return words.loc.as_matrix()
#
# # #to find the closest word to a vector:
# #
# # words_matrix = words.as_matrix()
# #
# # def find_closest_word(v):
# #   diff = words_matrix - v
# #   delta = np.sum(diff * diff, axis=1)
# #   i = np.argmin(delta)
# #   return words.iloc[i].name

# def myAct(out):
#     return K.softmax(K.tanh(out))

def build_encoder_model(embedding,shape):

    model_Summ = Sequential()
    #model_Summ.add(Embedding(max_enc_steps, embedding_vecor_length, input_length=max_dec_steps))
    model_Summ.add(Embedding(input_dim=embedding.shape[0],output_dim=embedding.shape[1],weights=[embedding],name='embedding_glove'))

    lstm = LSTM(units=shape[1],input_shape=shape,name='lstm_1',return_sequences=True, activation='softmax')

    model_Summ.add(lstm)
    model_Summ.add(Dropout(dropout,name='dropout_1'))
    lstm = LSTM(units=shape[1],input_shape=shape,name='lstm_2',activation='softmax',return_sequences=True)

    model_Summ.add(lstm)
    model_Summ.add(Dropout(dropout,name='dropout_2'))
    model_Summ.add(Dense(units=shape[1]))

    #model_Summ.add(Dense(activity_regularizer=lrate))
    model_Summ.add(Activation('softmax',name='activation'))

    return model_Summ



def build_decoder_model(embedding,x_train,y_train):
    rnn_size = [x_train[1:]]
    model_Summ = Sequential()
    model_Summ.add(Embedding(input_dim=hidden_dim,output_dim=y_train.shape,weights=[embedding],name='embedding_glove'))
    for i in range(2):
        lstm = LSTM(rnn_size,name='lstm_%d' %(i+1))
        model_Summ.add(lstm)
        model_Summ.add(Dropout(dropout,name="dropout_%d" %(i+1)))
    model_Summ.add(Dense())
    model_Summ.add(Activation('softmax',name='activation'))
    return model_Summ









# def get_embedding(word_embeddings, embeddings_index, vocab):
#
#     # coefs = np.asarray(values[1:], dtype='float32')
#     # embeddings_index[word] = coefs
#     for word,i in vocab.items():
#         embedding_vector = embeddings_index.get(word)
#         if embedding_vector is not None:
#             # words not found in embedding index will be all-zeros.
#             word_embeddings[i] = embedding_vector
#
#     return word_embeddings

if __name__ == '__main__':
    main()
