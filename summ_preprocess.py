
dataset_path='/Tmp/bastienf/aclImdb/'
import nltk
from nltk import WhitespaceTokenizer
import numpy
import _pickle as pkl
from keras.preprocessing.text import Tokenizer,text_to_word_sequence
from nltk import WordPunctTokenizer
from collections import OrderedDict

import glob
import os

from subprocess import Popen, PIPE

# tokenizer.perl is from Moses: https://github.com/moses-smt/mosesdecoder/tree/master/scripts/tokenizer
#tokenizer_cmd = ['./tokenizer.perl', '-l', 'en', '-q', '-']


# def tokenize(sentences):
#     text = "\n".join(sentences)
#     tokenizer = Popen(tokenizer_cmd, stdin=PIPE, stdout=PIPE)
#     tok_text, _ = tokenizer.communicate(text)
#     toks = tok_text.split('\n')[:-1]
#     print ('Done')
#
#     return toks


def build_dict(full_sentences):
    sentences = []
    # currdir = os.getcwd()
    # os.chdir('raw_data/')
    # for ff in glob.glob("*.jsonl"):
    #     with open(ff, 'r') as f:
    #         sentences.append(f.readline().strip())
    # # os.chdir('%s/neg/' % path)
    # # for ff in glob.glob("*.txt"):
    # #     with open(ff, 'r') as f:
    # #         sentences.append(f.readline().strip())
    # os.chdir(currdir)

    for ss in full_sentences:
        sentences.append(text_to_word_sequence(ss))

    print ('Building dictionary..',)
    wordcount = dict()
    for ss in sentences:
        # words = ss.strip().lower().split()
        for w in ss:
            if w not in wordcount:
                wordcount[w] = 1
            else:
                wordcount[w] += 1

    counts = wordcount.values()
    keys = wordcount.keys()


    #
    # print('keys,..')
    # print(keys)

    # sorted_idx = numpy.argsort(counts)[::-1]

    # worddict = dict()
    #
    # for idx, ss in enumerate(sorted_idx):
    #     print(idx)
    #     print(ss)
    #     worddict[keys] = idx+2  # leave 0 and 1 (UNK)

    print (numpy.sum(counts), ' total words ', len(keys), ' unique words')

    return wordcount


def grab_data(sentences, dictionary):
    # sentences = []
    # currdir = os.getcwd()
    # os.chdir(path)
    # for ff in glob.glob("*.jsonl"):
    #     with open(ff, 'r') as f:
    #         sentences.append(f.readline().strip())
    # os.chdir(currdir)
    # sentences = tokenize(sentences)

    seqs = [None] * len(sentences)
    for idx, ss in enumerate(sentences):
        words = ss.strip().lower().split()
        seqs[idx] = [dictionary[w] if w in dictionary else 1 for w in words]

    return seqs

def grab_data_tokenize(sentences):
    for ss in sentences:
        tokenized_sentences = text_to_word_sequence(ss)
    return tokenized_sentences

def build_pickle(article, abstract):


    # article_dictionary = build_dict(article)
    # abstract_dictionary = build_dict(abstract)

    abstract = [nltk.word_tokenize(h)for h in abstract]

    article = [nltk.word_tokenize(h)for h in article]

    abstract = [' '.join(h) for h in abstract]
    article = [' '.join(h) for h in article]
    print(abstract)
    dictionary = build_dict((article + abstract))
    # train_x = grab_data(article, article_dictionary)
    # train_y = grab_data(abstract,abstract_dictionary)
    #
    # train_x = grab_data_tokenize(article)
    # train_y = grab_data_tokenize(abstract)
    # #train_x_neg = grab_data(path+'train/neg', dictionary)
    # #train_x = train_x_pos + train_x_neg
    # #train_y = [1] * len(train_x_pos) + [0] * len(train_x_neg)
    # test_x = grab_data_tokenize(article)
    # #test_x = grab_data(article, article_dictionary)
    # #test_x_neg = grab_data(path+'test/neg', dictionary)
    # #test_x = test_x_pos + test_x_neg
    # test_y = grab_data_tokenize(abstract)

    f = open('processed_data/tokens.pkl', 'wb')
    pkl.dump((abstract, article), f, -1)
    #pkl.dump((test_x, test_y), f, -1)
    f.close()

    f = open('processed_data/tokens.dict.pkl', 'wb')
    pkl.dump(dictionary, f, -1)
    f.close()