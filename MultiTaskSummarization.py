from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.layers.embeddings import Embedding
from keras.layers import Input, Dense

from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential,Model
from keras.layers import Dense, LSTM, TimeDistributed, Reshape
import numpy as np
import json
from keras.preprocessing.text import Tokenizer
from keras.utils import  to_categorical
from snli_preprocessing_ne import extract_tokens_from_binary_parse, yield_examples,filter_article_abstract



############################ (Variables are captured from the paper)
RNNSize=512
WordEmbeddingDimension=256
InputVocabSize=68885
TargetVocabSize=68885
SentenceSize=300
DocMaxLengthEntail = 50
VocabSize = 2000
EntailmentOutputDim=3



def yield_examples(fn, skip_no_majority=True, limit=None):
    for i, line in enumerate(open(fn)):
        if limit and i > limit:
            break
        data = json.loads(line)
        label = data['gold_label']
        s1 = ' '.join(extract_tokens_from_binary_parse(data['sentence1_binary_parse']))
        s2 = ' '.join(extract_tokens_from_binary_parse(data['sentence2_binary_parse']))
        if skip_no_majority and label == '-':
            continue
        yield (label, s1, s2)


#The function below load the data and convert Y to categorial output
def load_data(fn, limit=None):           
    raw_data = list(yield_examples(fn=fn, limit=limit))
    left = [s1 for _, s1, s2 in raw_data]
    right = [s2 for _, s1, s2 in raw_data]
    LABELS = {'contradiction': 0, 'neutral': 1, 'entailment': 2}
    Y = [];
    for l, s1, s2 in raw_data:
        if l == 'entailment':
            Y.append(2)
        elif l == 'contradiction':
            Y.append(0)
        elif l == 'neutral':
            Y.append(1)

    Y = to_categorical(Y, len(LABELS))
    return left,right, Y

def encodeDocumentsToVector(VocabSize,Docs):
    encoded_docs = [one_hot(d, VocabSize) for d in Docs]
    padded_docs = pad_sequences(encoded_docs, maxlen=DocMaxLengthEntail, padding='post')
    return padded_docs;

def tokenizeInputAndOutput(article,abstract):
    indexer = Tokenizer(lower=True, filters='')
    indexer.fit_on_texts(article + abstract)
    return indexer;

def addPaddingToData(indexer,Data,lengthToPad):
    return pad_sequences(sequences=indexer.texts_to_sequences(Data), maxlen=lengthToPad, padding='post')


def prep_summarization_data():
    article, abstract = filter_article_abstract('article_data/sample.jsonl')   #Change the path to whatever dataset you want to point to for loading the articles
    return article,abstract;

def prepareInputAndOutputDataForModel(indexer, SentenceSize, inputData, outputData):
    articlePaddedData = addPaddingToData(indexer, inputData, SentenceSize);
    abstractPaddedData = np.zeros(shape=(articlePaddedData.shape[0], TargetVocabSize))

    outputDataIndex = 0;
    for eachLineOutputData in outputData:
        wordArray = [];
        outputLineToProcess=[];
        outputLineToProcess.append(eachLineOutputData)
        tokenizer = Tokenizer(lower=True, filters=' ')
        tokenizer.fit_on_texts(outputLineToProcess);

        for word in tokenizer.word_docs.keys():
            wordArray.append(word)
        for word in wordArray:
            indexOfWord = indexer.word_index.get(word)
            if indexOfWord is not None:
                abstractPaddedData[outputDataIndex,indexOfWord]=1;
        outputDataIndex = outputDataIndex + 1;
    return articlePaddedData, abstractPaddedData;



# I have defined two models below 1) Text summary model 2) entailment model
# Both follows encoder and decoder architecture but IMPORTANT POINT is that both shares the same DECODER LAYER. you can see below.

########################################### (TEXT SUMMARY MODEL)

print("Loading summary and abstract data");

article,abstract = prep_summarization_data();
indexer = tokenizeInputAndOutput(article, abstract);

#here we are updating input and output vocabulary size of default

InputVocabSize = len(indexer.word_counts) +1;
TargetVocabSize = len(indexer.word_counts) + 1

summaryEncoderLayer = LSTM(RNNSize,  return_sequences=True)
commonDecoderLayer = LSTM(RNNSize)
summaryOutputLayer = (Dense(TargetVocabSize, activation='sigmoid'));  #Sigmoid to make sure the probability are between 0 and 1

summaryModel = Sequential()
summaryModel.add(Embedding(InputVocabSize,WordEmbeddingDimension, input_length=SentenceSize));
summaryModel.add(summaryEncoderLayer);
summaryModel.add(commonDecoderLayer);
summaryModel.add(summaryOutputLayer);
summaryModel.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])



############################################## (Entailment Model)


entailInput=Input(shape=(50,))   #you can change the shape according to your need (Shape=(50,0) means 50 words)
embeddLayer = Embedding(InputVocabSize, 8, input_length=DocMaxLengthEntail)(entailInput)
entailInput2=Input(shape=(50,))  # Same as above  
entailmentEncoder = LSTM(RNNSize, return_sequences= True)(embeddLayer)
commonDecoderLayer=commonDecoderLayer(entailmentEncoder)
modelOutput= Dense(EntailmentOutputDim, activation='sigmoid')(commonDecoderLayer);

EntailmentModel = Model(inputs=[entailInput, entailInput2], outputs=[modelOutput])  # Given premise and hypothesis predict the result is contradiction, neutral or entailment
EntailmentModel.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])




################## Data preparation for both model

print("Data preparation started !")
articlePaddedData, abstractPaddedData = prepareInputAndOutputDataForModel(indexer, SentenceSize, article, abstract)
left,right,Y = (load_data('snil_data/snli_1.0_dev.jsonl'));       #Here I put dummy json file to load, point to the bigger dataset of snil
PremiseEncoded = encodeDocumentsToVector(TargetVocabSize,left);
HypothesisEncoded = encodeDocumentsToVector(TargetVocabSize, right);
EntailmentEncodedOutput = Y;



################# Train models in chunks alternatively (Performing Multitask Learning)

finishTraining = False;
SummaryOffset=3;  #Change the summary offset to 100 and Entailment offset to 5, according to paper
EntailmentOffset=5;

SummaryIndex=0;
EntailmentIndex=0;

print("Training has been started ! ")

while( not finishTraining ):

    if(((SummaryOffset + SummaryIndex) >= (articlePaddedData.shape[0] - 1)) or (EntailmentIndex + EntailmentOffset) >= (PremiseEncoded.shape[0]-1) ):
        finishTraining = True

    if(not finishTraining):

        print(SummaryIndex)
        print(SummaryIndex + SummaryOffset)
        articleTrainingPadded = articlePaddedData[SummaryIndex : SummaryIndex+SummaryOffset];
        summaryTrainingPadded = abstractPaddedData[SummaryIndex : SummaryIndex + SummaryOffset];

        premiseEncodedPadded = PremiseEncoded[EntailmentIndex : EntailmentIndex + EntailmentOffset]
        hypothesisEncodedPadded = HypothesisEncoded[EntailmentIndex : EntailmentIndex + EntailmentOffset]
        EntailmentEncodedPadded= EntailmentEncodedOutput[EntailmentIndex : EntailmentIndex + EntailmentOffset]

        print("Training summary model started now - ")
        summaryModel.fit(articleTrainingPadded,summaryTrainingPadded, batch_size=20)
        print("Training entailment model started now - ")
        EntailmentModel.fit([premiseEncodedPadded, hypothesisEncodedPadded], EntailmentEncodedPadded, epochs=50, verbose=0)
        SummaryIndex = SummaryIndex + SummaryOffset
        EntailmentIndex = EntailmentIndex + EntailmentOffset

print("finished training !")


############################################## (The code below used for prediction of summary)

# ! WARNING = Don't touch this part till you don't have enough traing otherwise you will see weired stuff
# This part is to generate summary

# PredicatedResult = (summaryModel.predict(articlePaddedData[0:1]))
# PredicatedIndexes = np.argsort(PredicatedResult[0])[::-1][:TargetVocabSize]
# PredicatedIndexes = PredicatedIndexes[0:100]
# res = dict((v,k) for k,v in indexer.word_index.items())
# for indexes in PredicatedIndexes:
#     print(res.get(indexes))
#
# print('Entailment output')
# print(EntailmentModel.predict([PremiseEncoded[0:1], HypothesisEncoded[0:1]]))
#

##############################################