import json
from keras.utils.np_utils import to_categorical
import os
import re
import time
import numpy as np
import tensorflow as tf
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict

import keras
from keras.models import Sequential
import keras.backend as K
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.layers import merge, recurrent, Dense, Input, Dropout, TimeDistributed, Activation
from keras.layers.embeddings import Embedding
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Lambda
from keras.layers.wrappers import Bidirectional
from keras.models import Model
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.regularizers import l2
from keras.utils import np_utils
from keras.layers.recurrent import GRU, LSTM
from keras.backend.tensorflow_backend import set_session
from keras.engine.topology import Layer

from snli_preprocessing_ne import extract_tokens_from_binary_parse, yield_examples,filter_article_abstract


def time_count(fn):
    # Funtion wrapper used to memsure time consumption
    def _wrapper(*args, **kwargs):
        start = time.clock()
        returns = fn(*args, **kwargs)
        print("[time_count]: %s cost %fs" % (fn.__name__, time.clock() - start))
        return returns

    return _wrapper


class AttentionAlignmentModel:
    def __init__(self, annotation='biGRU', dataset='snli'):
        # 1, Set Basic Model Parameters
        self.Layers = 1
        self.EmbeddingSize = 300
        self.BatchSize = 2
        self.Patience = 8
        self.MaxEpoch = 3
        self.SentMaxLen = 42
        self.DropProb = 0.4
        self.L2Strength = 1e-5
        self.Activate = 'relu'
        self.Optimizer = 'rmsprop'
        self.rnn_type = annotation
        self.dataset = dataset

        # 2, Define Class Variables
        self.Vocab = 0
        self.model = None
        self.GloVe = defaultdict(np.array)
        self.indexer, self.Embed = None, None
        self.train, self.validation, self.test = [], [], []
        self.article, self.article_test, self.abstract, self.abstract_test= [], [], [],[]
        self.Labels = {'contradiction': 0, 'neutral': 1, 'entailment': 2}
        self.rLabels = {0: 'contradiction', 1: 'neutral', 2: 'entailment'}

        #Define model variables to store encoder models
        self.model_ent, self.model_summ = None, None

    def extract_tokens_from_binary_parse(parse):
        return parse.replace('(', ' ').replace(')', ' ').replace('-LRB-', '(').replace('-RRB-', ')').split()

    def yield_examples(fn, skip_no_majority=True, limit=None):
        for i, line in enumerate(open(fn)):
            if limit and i > limit:
                break
            data = json.loads(line)
            label = data['gold_label']
            s1 = ' '.join(extract_tokens_from_binary_parse(data['sentence1_binary_parse']))
            s2 = ' '.join(extract_tokens_from_binary_parse(data['sentence2_binary_parse']))
            if skip_no_majority and label == '-':
                continue
            yield (label, s1, s2)

    def load_data(self,fn, limit=None):
        raw_data = list(yield_examples(fn=fn, limit=limit))
        left = [s1 for _, s1, s2 in raw_data]
        # right = [s2 for _, s1, s2 in raw_data]
        print('total length')
        print(len(left))
        #LABELS = {'contradiction': 0, 'neutral': 1, 'entailment': 2}
        left = [s1 for l, s1, s2 in raw_data if l is 'entailment']
        print('reduced length')
        print(len(left))
        right = [s2 for l, s1, s2 in raw_data if l is 'entailment']
        #Y = np.array([2 for l, s1, s2 in raw_data if l is 'entailment'])
        #Y = to_categorical(Y, len(LABELS))
        #return Y
        return left, right

    #    training = get_data('snli_1.0_train.jsonl')
    #    validation = get_data('snli_1.0_dev.jsonl')
    #    test = get_data('snli_1.0_test.jsonl')


    # train = load_data('snli_1.0/snli_1.0_train.jsonl')

    @time_count
    def prep_summarization_data(self):
        self.article, self.abstract = filter_article_abstract('raw_data/signalmedia-1m.jsonl/sample.jsonl')
        # 2, Prep Word Indexer: assign each word a number
        self.indexer = Tokenizer(lower=False, filters='')
        self.indexer.fit_on_texts(self.article + self.abstract)
        self.Vocab = len(self.indexer.word_counts) + 1

        #Convert each word in sent to num and zero pad
        def padding(x, MaxLen):
            return pad_sequences(sequences=self.indexer.texts_to_sequences(x), maxlen=MaxLen)

        def pad_data(x):
            return padding(x, self.SentMaxLen)

        self.article = pad_data(self.article)
        self.abstract = pad_data(self.abstract)

        self.article, self.article_test, self.abstract, self.abstract_test = train_test_split(self.article,self.abstract)


    @time_count
    def prep_entailment_data(self):
        # 1, Read raw Training,Validation and Test data
        self.train = self.load_data('snli_1.0/snli_1.0_train.jsonl',limit=1000)
        self.validation = self.load_data('snli_1.0/snli_1.0_dev.jsonl',limit=100)
        self.test = self.load_data('snli_1.0/snli_1.0_test.jsonl',limit=50)
        # 2, Prep Word Indexer: assign each word a number
        self.indexer = Tokenizer(lower=False, filters='')
        self.indexer.fit_on_texts(self.train[0] + self.train[1])  # todo remove test
        self.Vocab = len(self.indexer.word_counts) + 1

        # 3, Convert each word in sent to num and zero pad
        def padding(x, MaxLen):
            return pad_sequences(sequences=self.indexer.texts_to_sequences(x), maxlen=MaxLen)

        def pad_data(x):
            return padding(x[0], self.SentMaxLen), padding(x[1], self.SentMaxLen)

        self.train = pad_data(self.train)
        print(self.train[0].shape)
        self.validation = pad_data(self.validation)
        self.test = pad_data(self.test)

    def load_GloVe(self):
        # Creat a embedding matrix for word2vec(use GloVe)
        embed_index = {}

        for line in open('glove.6B.300d.txt', 'r', encoding='UTF-8', errors='ignore'):
            value = line.split(' ')  # Warning: Can't use split()! I don't know why...
            word = value[0]
            embed_index[word] = np.asarray(value[1:], dtype='float32')
        embed_matrix = np.zeros((self.Vocab, self.EmbeddingSize))
        unregistered = []
        for word, i in self.indexer.word_index.items():
            vec = embed_index.get(word)
            if vec is None:
                unregistered.append(word)
            else:
                embed_matrix[i] = vec
        np.save('GloVe_mtl.npy', embed_matrix)
        open('unregisterd_word.txt', 'w').write(str(unregistered))

    def load_GloVe_dict(self):
        for line in open('glove.6B.300d.txt', 'r', encoding='UTF-8', errors='ignore'):
            value = line.split(' ')  # Warning: Can't use split()! I don't know why...
            word = value[0]
            self.GloVe[word] = np.asarray(value[1:], dtype='float32')

    @time_count
    def prep_embd(self):
        # Add a Embed Layer to convert word index to vector
        if not os.path.exists('GloVe_mtl.npy'):
            self.load_GloVe()
        embed_matrix = np.load('GloVe_mtl.npy')
        self.Embed = Embedding(input_dim=self.Vocab,
                               output_dim=self.EmbeddingSize,
                               input_length=self.SentMaxLen,
                               trainable=False,
                               weights=[embed_matrix],
                               name='embed_snli')

    def create_standard_attention_decoder_model(self,task1_input,task2_input, task1_output, task2_output):

        self.model = Model(inputs=[task1_input, task2_input], outputs=[task1_output, task2_output])
        # 4, Concat original and alignment, score each pair of alignment
        # final = keras.models.con
        # article= keras.layers.concatenate([embed_art, article])  # [batch_size, PreLen, 2*Size]
        # abstract = keras.layers.concatenate([embed_abs, abstract])  # [batch_size, Hypo, 2*Size]
        # hypothesis = keras.layers.concatenate([embed_h, hypothesis])  # [batch_size, Hypo, 2*Size]
        # premise = keras.layers.concatenate([embed_p, premise])  # [batch_size, Hypo, 2*Size]
        # for i in range(2):
        #     scoreG = TimeDistributed(Dense(200,
        #                                    activation='relu',
        #                                    kernel_regularizer=l2(self.L2Strength),
        #                                    bias_regularizer=l2(self.L2Strength)))
        #     article = scoreG(article)  # [batch_size, Psize, units]
        #     abstract_hypo = scoreG(abstract_hypo)  # [batch_size, Hsize, units]
        #     article_prem = Dropout(self.DropProb)(article_prem)
        #     abstract_hypo = Dropout(self.DropProb)(abstract_hypo)
        #
        # # 5, Sum all these scores, and make final judge according to sumed-score
        # SumWords = Lambda(lambda X: K.reshape(K.sum(X, axis=1, keepdims=True), (-1, 200)))
        # V_P = SumWords(article_prem)  # [batch_size, 512]
        # V_H = SumWords(abstract_hypo)  # [batch_size, 512]
        # final = keras.layers.concatenate([V_P, V_H])
        # for i in range(2):
        #     final = Dense(200,
        #                   activation='relu',
        #                   kernel_regularizer=l2(self.L2Strength),
        #                   bias_regularizer=l2(self.L2Strength))(final)
        #     final = Dropout(self.DropProb)(final)
        #     final = BatchNormalization()(final)
        #
        # # 6, Prediction by softmax
        # final = Dense(3,activation='softmax')(final)
        # model = Model(inputs=[main_input, auxiliary_input], outputs=[main_output, auxiliary_output])



        #model = Model(inputs=[main_input, auxiliary_input], outputs=[main_output, auxiliary_output])



    def create_standard_attention_encoder_model_entailment(self):

        premise = Input(shape=(self.SentMaxLen,), dtype='int32')
        #self.model_ent = Sequential()
        embed_p = self.Embed(premise)  # [batchsize, Psize, Embedsize]
        #self.model_ent.add(embed_p)
        for i in range(2):
            lstm = LSTM(200, name='lstm_ent_%d' % (i + 1),return_sequences=True)
            #self.model_ent.add(lstm)
            EmbdProject = TimeDistributed(Dense(200,
                                                activation='relu',
                                                kernel_regularizer=l2(self.L2Strength),
                                                bias_regularizer=l2(self.L2Strength)))
            embed_p = Dropout(self.DropProb)(lstm(embed_p))
            embed_p = Dropout(self.DropProb)(EmbdProject(embed_p))  # [batchsize, Psize, units]
        embed_p = Lambda(lambda x: keras.activations.softmax(x))(embed_p)
        return premise,embed_p
            #self.model_ent.add(embed_p)
        #self.model_ent.add(Activation('softmax', name='activation'))



        # # 1, Embedding the input and project the embeddings
        # premise = Input(shape=(self.SentMaxLen,), dtype='int32')
        # #hypothesis = Input(shape=(self.SentMaxLen,), dtype='int32')
        # embed_p = self.Embed(premise)  # [batchsize, Psize, Embedsize]
        # #embed_h = self.Embed(hypothesis)  # [batchsize, Hsize, Embedsize]
        # EmbdProject = TimeDistributed(Dense(200,
        #                                     activation='relu',
        #                                     kernel_regularizer=l2(self.L2Strength),
        #                                     bias_regularizer=l2(self.L2Strength)))
        # embed_p = Dropout(self.DropProb)(EmbdProject(embed_p))  # [batchsize, Psize, units]
        # #embed_h = Dropout(self.DropProb)(EmbdProject(embed_h))  # [batchsize, Hsize, units]
        #
        # # 2, Score each embeddings and calc score matrix Eph.
        # #F_p, F_h = embed_p, embed_h
        # F_p= embed_p
        # for i in range(2):  # Applying Decomposable Score Function
        #     scoreF = TimeDistributed(Dense(200,
        #                                    activation='relu',
        #                                    kernel_regularizer=l2(self.L2Strength),
        #                                    bias_regularizer=l2(self.L2Strength)))
        #     F_p = Dropout(self.DropProb)(scoreF(F_p))  # [batch_size, Psize, units]
            #F_h = Dropout(self.DropProb)(scoreF(F_h))  # [batch_size, Hsize, units]
        #Eph = keras.layers.Dot(axes=(2, 2))([F_p, F_h])  # [batch_size, Psize, Hsize]

        # 3, Normalize score matrix and get alignment
        # Ep = Lambda(lambda x: keras.activations.softmax(x))(Eph)  # [batch_size, Psize, Hsize]
        # Eh = keras.layers.Permute((2, 1))(Eph)  # [batch_size, Hsize, Psize)
        # Eh = Lambda(lambda x: keras.activations.softmax(x))(Eh)  # [batch_size, Hsize, Psize]
        # PremAlign = keras.layers.Dot((2, 1))([Ep, embed_h])
        # HypoAlign = keras.layers.Dot((2, 1))([Eh, embed_p])

        # # 4, Concat original and alignment, score each pair of alignment
        # PremAlign = keras.layers.concatenate([embed_p, PremAlign])  # [batch_size, PreLen, 2*Size]
        # HypoAlign = keras.layers.concatenate([embed_h, HypoAlign])  # [batch_size, Hypo, 2*Size]
        # for i in range(2):
        #     scoreG = TimeDistributed(Dense(200,
        #                                    activation='relu',
        #                                    kernel_regularizer=l2(self.L2Strength),
        #                                    bias_regularizer=l2(self.L2Strength)))
        #     PremAlign = scoreG(PremAlign)  # [batch_size, Psize, units]
        #     HypoAlign = scoreG(HypoAlign)  # [batch_size, Hsize, units]
        #     PremAlign = Dropout(self.DropProb)(PremAlign)
        #     HypoAlign = Dropout(self.DropProb)(HypoAlign)
        # return PremAlign, HypoAlign, embed_p, embed_h

    def create_standard_attention_encoder_model_summarization(self):
        article = Input(shape=(self.SentMaxLen,), dtype='int32')
        #self.model_summ = Sequential()
        embed_art = self.Embed(article)  # [batchsize, Psize, Embedsize]
        #self.model_summ.add(embed_art)
        for i in range(2):
            lstm = LSTM(200, name='lstm_summ_%d' % (i + 1),return_sequences=True)
            #self.model_summ.add(lstm)
            EmbdProject = TimeDistributed(Dense(200,
                                                activation='relu',
                                                kernel_regularizer=l2(self.L2Strength),
                                                bias_regularizer=l2(self.L2Strength)))
            embed_art = Dropout(self.DropProb)(lstm(embed_art))
            embed_art = Dropout(self.DropProb)(EmbdProject(embed_art))  # [batchsize, Psize, units]
            #self.model_summ.add(embed_art)
        #self.model_summ.add(Activation('softmax', name='activation'))
        embed_art = Lambda(lambda x: keras.activations.softmax(x))(embed_art)
        return article,embed_art
        # # 1, Embedding the input and project the embeddings
        # article = Input(shape=(self.SentMaxLen,), dtype='int32')
        # abstract = Input(shape=(self.SentMaxLen,), dtype='int32')
        # embed_art = self.Embed(article)  # [batchsize, Psize, Embedsize]
        # embed_abs = self.Embed(abstract)  # [batchsize, Hsize, Embedsize]
        # EmbdProject = TimeDistributed(Dense(200,
        #                                     activation='relu',
        #                                     kernel_regularizer=l2(self.L2Strength),
        #                                     bias_regularizer=l2(self.L2Strength)))
        # embed_art = Dropout(self.DropProb)(EmbdProject(embed_art))  # [batchsize, Psize, units]
        # embed_abs = Dropout(self.DropProb)(EmbdProject(embed_abs))  # [batchsize, Hsize, units]
        #
        # # 2, Score each embeddings and calc score matrix Eph.
        # F_p, F_h = embed_art, embed_abs
        # for i in range(2):  # Applying Decomposable Score Function
        #     scoreF = TimeDistributed(Dense(200,
        #                                    activation='relu',
        #                                    kernel_regularizer=l2(self.L2Strength),
        #                                    bias_regularizer=l2(self.L2Strength)))
        #     F_p = Dropout(self.DropProb)(scoreF(F_p))  # [batch_size, Psize, units]
        #     F_h = Dropout(self.DropProb)(scoreF(F_h))  # [batch_size, Hsize, units]
        # Eph = keras.layers.Dot(axes=(2, 2))([F_p, F_h])  # [batch_size, Psize, Hsize]
        #
        # # 3, Normalize score matrix and get alignment
        # Ep = Lambda(lambda x: keras.activations.softmax(x))(Eph)  # [batch_size, Psize, Hsize]
        # Eh = keras.layers.Permute((2, 1))(Eph)  # [batch_size, Hsize, Psize)
        # Eh = Lambda(lambda x: keras.activations.softmax(x))(Eh)  # [batch_size, Hsize, Psize]
        # articleAlign = keras.layers.Dot((2, 1))([Ep, embed_art])
        # abstractAlign = keras.layers.Dot((2, 1))([Eh, embed_abs])
        #
        # # 4, Concat original and alignment, score each pair of alignment
        # articleAlign = keras.layers.concatenate([embed_art, articleAlign])  # [batch_size, PreLen, 2*Size]
        # abstractAlign = keras.layers.concatenate([embed_abs, abstractAlign])  # [batch_size, Hypo, 2*Size]
        # for i in range(2):
        #     scoreG = TimeDistributed(Dense(200,
        #                                    activation='relu',
        #                                    kernel_regularizer=l2(self.L2Strength),
        #                                    bias_regularizer=l2(self.L2Strength)))
        #     articleAlign = scoreG(articleAlign)  # [batch_size, Psize, units]
        #     abstractAlign = scoreG(abstractAlign)  # [batch_size, Hsize, units]
        #     articleAlign = Dropout(self.DropProb)(articleAlign)
        #     abstractAlign = Dropout(self.DropProb)(abstractAlign)
        #
        # return articleAlign, abstractAlign, embed_art, embed_abs


    @time_count
    def compile_model(self):
        """ Load Possible Existing Weights and Compile the Model """
        self.model.compile(optimizer=self.Optimizer,
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])
        self.model.summary()
        fn = self.rnn_type + '.check'
        if os.path.exists(fn):
            self.model.load_weights(fn, by_name=True)
            print('--------Load Weights Successful!--------')

    def evaluate_on_test(self):
        loss, acc = self.model.evaluate([self.test[0], self.article_test],
                                        [self.test[1],self.abstract_test], batch_size=self.BatchSize)
        print("Test: loss = {:.5f}, acc = {:.3f}%".format(loss, acc))

    def start_train(self):
        """ Starts to Train the entire Model Based on set Parameters """
        # 1, Prep
        callback = [EarlyStopping(patience=self.Patience),
                    ReduceLROnPlateau(patience=5, verbose=1),
                    CSVLogger(filename=self.rnn_type + 'log.csv'),
                    ModelCheckpoint(self.rnn_type + '.check',
                                    save_best_only=True,
                                    save_weights_only=True)]

        # 2, Train
        #self.model.train_on_batch()

        self.model.fit([self.train[0],self.article], [self.train[1],self.abstract],
                  epochs=self.MaxEpoch, batch_size=self.BatchSize,
                callbacks = callback,validation_data = ([self.test[0],self.article_test],[self.test[1],self.abstract_test]))
        # self.model.fit(x=[self.train[0], self.train[1]],
        #                y=self.train[2],
        #                batch_size=self.BatchSize,

        #                epochs=self.MaxEpoch,
        #                validation_data=([self.test[0], self.test[1]], self.test[2]),
        #                callbacks=callback)

        # 3, Evaluate
        self.model.load_weights(self.rnn_type + '.check')  # revert to the best model
        self.evaluate_on_test()

    @staticmethod
    def plotHeatMap(df, psize=(8, 8), filename='Heatmap'):
        ax = sns.heatmap(df, vmax=.85, square=True, cbar=False, annot=True)
        plt.xticks(rotation=40), plt.yticks(rotation=360)
        fig = ax.get_figure()
        fig.set_size_inches(psize)
        fig.savefig(filename)
        plt.clf()





if __name__ == '__main__':

    md = AttentionAlignmentModel(annotation='EAM', dataset='snli')
    print('preparing data...')
    md.prep_entailment_data() #----> working
    md.prep_summarization_data() #--> working
    print('data prepared...')
    print('preparing embedding...')
    md.prep_embd()
    print('embeddings prepared...')
    # _test = False
    # # md.create_model(test_mode = _test)a
    # # md.create_standard_attention_model()
    print('preparing model...')
    input_task1, output_task1 = md.create_standard_attention_encoder_model_entailment()

    input_task2, output_task2 = md.create_standard_attention_encoder_model_summarization()

    md.create_standard_attention_decoder_model(input_task1,input_task2,output_task1, output_task2)
    print('model prepared...')
    print('compiling model...')
    md.compile_model()
    print('model compiled...')
    # # md.label_test_file()
    print('training.....')
    md.start_train()
    print('TRAINING COMPLETE!!!!!')
    # print('evaluating...')
    # md.evaluate_on_test()
    # print('evaluation done....')



