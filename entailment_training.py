import json
from keras.utils.np_utils import to_categorical
import os
import re
import time
import numpy as np
import tensorflow as tf
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict

import keras
import keras.backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.layers import merge, recurrent, Dense, Input, Dropout, TimeDistributed
from keras.layers.embeddings import Embedding
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Lambda
from keras.layers.wrappers import Bidirectional
from keras.models import Model
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.regularizers import l2
from keras.utils import np_utils
from keras.layers.recurrent import GRU, LSTM
from keras.backend.tensorflow_backend import set_session
from keras.engine.topology import Layer

from snli_preprocessing_ne import extract_tokens_from_binary_parse, yield_examples


def time_count(fn):
    # Funtion wrapper used to memsure time consumption
    def _wrapper(*args, **kwargs):
        start = time.clock()
        returns = fn(*args, **kwargs)
        print("[time_count]: %s cost %fs" % (fn.__name__, time.clock() - start))
        return returns

    return _wrapper


class AttentionAlignmentModel:
    def __init__(self, annotation='biGRU', dataset='snli'):
        # 1, Set Basic Model Parameters
        self.Layers = 1
        self.EmbeddingSize = 300
        self.BatchSize = 512
        self.Patience = 8
        self.MaxEpoch = 3
        self.SentMaxLen = 42
        self.DropProb = 0.4
        self.L2Strength = 1e-5
        self.Activate = 'relu'
        self.Optimizer = 'rmsprop'
        self.rnn_type = annotation
        self.dataset = dataset

        # 2, Define Class Variables
        self.Vocab = 0
        self.model = None
        self.GloVe = defaultdict(np.array)
        self.indexer, self.Embed = None, None
        self.train, self.validation, self.test = [], [], []
        self.Labels = {'contradiction': 0, 'neutral': 1, 'entailment': 2}
        self.rLabels = {0: 'contradiction', 1: 'neutral', 2: 'entailment'}

    def extract_tokens_from_binary_parse(parse):
        return parse.replace('(', ' ').replace(')', ' ').replace('-LRB-', '(').replace('-RRB-', ')').split()

    def yield_examples(fn, skip_no_majority=True, limit=None):
        for i, line in enumerate(open(fn)):
            if limit and i > limit:
                break
            data = json.loads(line)
            label = data['gold_label']
            s1 = ' '.join(extract_tokens_from_binary_parse(data['sentence1_binary_parse']))
            s2 = ' '.join(extract_tokens_from_binary_parse(data['sentence2_binary_parse']))
            if skip_no_majority and label == '-':
                continue
            yield (label, s1, s2)

    def load_data(self,fn, limit=None):
        raw_data = list(yield_examples(fn=fn, limit=limit))
        left = [s1 for _, s1, s2 in raw_data]
        right = [s2 for _, s1, s2 in raw_data]
        print(max(len(x.split()) for x in left))
        print(max(len(x.split()) for x in right))

        LABELS = {'contradiction': 0, 'neutral': 1, 'entailment': 2}
        Y = np.array([LABELS[l] for l, s1, s2 in raw_data])
        Y = to_categorical(Y, len(LABELS))
        #return Y
        return left, right, Y

    #    training = get_data('snli_1.0_train.jsonl')
    #    validation = get_data('snli_1.0_dev.jsonl')
    #    test = get_data('snli_1.0_test.jsonl')


    # train = load_data('snli_1.0/snli_1.0_train.jsonl')


    @time_count
    def prep_data(self):
        # 1, Read raw Training,Validation and Test data
        self.train = self.load_data('snli_1.0/snli_1.0_train.jsonl',limit=1000)
        self.validation = self.load_data('snli_1.0/snli_1.0_dev.jsonl',limit=100)
        self.test = self.load_data('snli_1.0/snli_1.0_test.jsonl',limit=50)
        # 2, Prep Word Indexer: assign each word a number
        self.indexer = Tokenizer(lower=False, filters='')
        self.indexer.fit_on_texts(self.train[0] + self.train[1])  # todo remove test
        self.Vocab = len(self.indexer.word_counts) + 1

        # 3, Convert each word in sent to num and zero pad
        def padding(x, MaxLen):
            return pad_sequences(sequences=self.indexer.texts_to_sequences(x), maxlen=MaxLen)

        def pad_data(x):
            return padding(x[0], self.SentMaxLen), padding(x[1], self.SentMaxLen), x[2]

        self.train = pad_data(self.train)
        self.validation = pad_data(self.validation)
        self.test = pad_data(self.test)

    def load_GloVe(self):
        # Creat a embedding matrix for word2vec(use GloVe)
        embed_index = {}

        for line in open('glove.6B.300d.txt', 'r', encoding='UTF-8', errors='ignore'):
            value = line.split(' ')  # Warning: Can't use split()! I don't know why...
            word = value[0]
            embed_index[word] = np.asarray(value[1:], dtype='float32')
        embed_matrix = np.zeros((self.Vocab, self.EmbeddingSize))
        unregistered = []
        for word, i in self.indexer.word_index.items():
            vec = embed_index.get(word)
            if vec is None:
                unregistered.append(word)
            else:
                embed_matrix[i] = vec
        np.save('GloVe_' + self.dataset + '.npy', embed_matrix)
        open('unregisterd_word.txt', 'w').write(str(unregistered))

    def load_GloVe_dict(self):
        for line in open('glove.6B.300d.txt', 'r', encoding='UTF-8', errors='ignore'):
            value = line.split(' ')  # Warning: Can't use split()! I don't know why...
            word = value[0]
            self.GloVe[word] = np.asarray(value[1:], dtype='float32')

    @time_count
    def prep_embd(self):
        # Add a Embed Layer to convert word index to vector
        if not os.path.exists('GloVe_' + self.dataset + '.npy'):
            self.load_GloVe()
        embed_matrix = np.load('GloVe_' + self.dataset + '.npy')
        self.Embed = Embedding(input_dim=self.Vocab,
                               output_dim=self.EmbeddingSize,
                               input_length=self.SentMaxLen,
                               trainable=False,
                               weights=[embed_matrix],
                               name='embed_snli')
        print('embedding values...')
        print(self.Vocab)
        print(self.EmbeddingSize)


    def create_standard_attention_model(self, test_mode=False):
        ''' This model is Largely based on [A Decomposable Attention Model, Ankur et al.] '''
        # 0, (Optional) Set the upper limit of GPU memory
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.2
        set_session(tf.Session(config=config))

        # 1, Embedding the input and project the embeddings
        premise = Input(shape=(self.SentMaxLen,), dtype='int32')
        hypothesis = Input(shape=(self.SentMaxLen,), dtype='int32')
        embed_p = self.Embed(premise)  # [batchsize, Psize, Embedsize]
        embed_h = self.Embed(hypothesis)  # [batchsize, Hsize, Embedsize]
        EmbdProject = TimeDistributed(Dense(200,
                                            activation='relu',
                                            kernel_regularizer=l2(self.L2Strength),
                                            bias_regularizer=l2(self.L2Strength)))
        embed_p = Dropout(self.DropProb)(EmbdProject(embed_p))  # [batchsize, Psize, units]
        embed_h = Dropout(self.DropProb)(EmbdProject(embed_h))  # [batchsize, Hsize, units]

        # 2, Score each embeddings and calc score matrix Eph.
        F_p, F_h = embed_p, embed_h
        for i in range(2):  # Applying Decomposable Score Function
            scoreF = TimeDistributed(Dense(200,
                                           activation='relu',
                                           kernel_regularizer=l2(self.L2Strength),
                                           bias_regularizer=l2(self.L2Strength)))
            F_p = Dropout(self.DropProb)(scoreF(F_p))  # [batch_size, Psize, units]
            F_h = Dropout(self.DropProb)(scoreF(F_h))  # [batch_size, Hsize, units]
        Eph = keras.layers.Dot(axes=(2, 2))([F_p, F_h])  # [batch_size, Psize, Hsize]

        # 3, Normalize score matrix and get alignment
        Ep = Lambda(lambda x: keras.activations.softmax(x))(Eph)  # [batch_size, Psize, Hsize]
        Eh = keras.layers.Permute((2, 1))(Eph)  # [batch_size, Hsize, Psize)
        Eh = Lambda(lambda x: keras.activations.softmax(x))(Eh)  # [batch_size, Hsize, Psize]
        PremAlign = keras.layers.Dot((2, 1))([Ep, embed_h])
        HypoAlign = keras.layers.Dot((2, 1))([Eh, embed_p])

        # 4, Concat original and alignment, score each pair of alignment
        PremAlign = keras.layers.concatenate([embed_p, PremAlign])  # [batch_size, PreLen, 2*Size]
        HypoAlign = keras.layers.concatenate([embed_h, HypoAlign])  # [batch_size, Hypo, 2*Size]
        for i in range(2):
            scoreG = TimeDistributed(Dense(200,
                                           activation='relu',
                                           kernel_regularizer=l2(self.L2Strength),
                                           bias_regularizer=l2(self.L2Strength)))
            PremAlign = scoreG(PremAlign)  # [batch_size, Psize, units]
            HypoAlign = scoreG(HypoAlign)  # [batch_size, Hsize, units]
            PremAlign = Dropout(self.DropProb)(PremAlign)
            HypoAlign = Dropout(self.DropProb)(HypoAlign)

        # 5, Sum all these scores, and make final judge according to sumed-score
        SumWords = Lambda(lambda X: K.reshape(K.sum(X, axis=1, keepdims=True), (-1, 200)))
        V_P = SumWords(PremAlign)  # [batch_size, 512]
        V_H = SumWords(HypoAlign)  # [batch_size, 512]
        final = keras.layers.concatenate([V_P, V_H])
        for i in range(2):
            final = Dense(200,
                          activation='relu',
                          kernel_regularizer=l2(self.L2Strength),
                          bias_regularizer=l2(self.L2Strength))(final)
            final = Dropout(self.DropProb)(final)
            final = BatchNormalization()(final)

        # 6, Prediction by softmax
        final = Dense(3 if self.dataset == 'snli' else 2,
                      activation='softmax')(final)
        if test_mode:
            self.model = Model(inputs=[premise, hypothesis], outputs=[Ep, Eh, final])
        else:
            self.model = Model(inputs=[premise, hypothesis], outputs=final)


    def create_enhanced_attention_model(self):
        # 0, (Optional) Set the upper limit of GPU memory
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        set_session(tf.Session(config=config))

        # 1, Embedding the input and project the embeddings
        premise = Input(shape=(self.SentMaxLen,), dtype='int32')
        hypothesis = Input(shape=(self.SentMaxLen,), dtype='int32')
        embed_p = self.Embed(premise)  # [batchsize, Psize, Embedsize]
        embed_h = self.Embed(hypothesis)  # [batchsize, Hsize, Embedsize]

        # 2, Encoder words with its surrounding context
        Encoder = Bidirectional(LSTM(units=300, return_sequences=True))
        embed_p = Dropout(self.DropProb)(Encoder(embed_p))
        embed_h = Dropout(self.DropProb)(Encoder(embed_h))

        # 2, Score each words and calc score matrix Eph.
        F_p, F_h = embed_p, embed_h
        Eph = keras.layers.Dot(axes=(2, 2))([F_h, F_p])  # [batch_size, Hsize, Psize]
        Eh = Lambda(lambda x: keras.activations.softmax(x))(Eph)  # [batch_size, Hsize, Psize]
        Ep = keras.layers.Permute((2, 1))(Eph)  # [batch_size, Psize, Hsize)
        Ep = Lambda(lambda x: keras.activations.softmax(x))(Ep)  # [batch_size, Psize, Hsize]

        # 4, Normalize score matrix, encoder premesis and get alignment
        PremAlign = keras.layers.Dot((2, 1))([Ep, embed_h])  # [-1, Psize, dim]
        HypoAlign = keras.layers.Dot((2, 1))([Eh, embed_p])  # [-1, Hsize, dim]
        mm_1 = keras.layers.Multiply()([embed_p, PremAlign])
        mm_2 = keras.layers.Multiply()([embed_h, HypoAlign])
        sb_1 = Lambda(lambda x: tf.subtract(x, PremAlign))(embed_p)
        sb_2 = Lambda(lambda x: tf.subtract(x, HypoAlign))(embed_h)

        PremAlign = keras.layers.Concatenate()([embed_p, PremAlign, sb_1, mm_1, ])  # [batch_size, Psize, 2*unit]
        HypoAlign = keras.layers.Concatenate()([embed_h, HypoAlign, sb_2, mm_2])  # [batch_size, Hsize, 2*unit]
        PremAlign = Dropout(self.DropProb)(PremAlign)
        HypoAlign = Dropout(self.DropProb)(HypoAlign)
        Compresser = TimeDistributed(Dense(300,
                                           kernel_regularizer=l2(self.L2Strength),
                                           bias_regularizer=l2(self.L2Strength),
                                           activation='relu'),
                                     name='Compresser')
        PremAlign = Compresser(PremAlign)
        HypoAlign = Compresser(HypoAlign)

        # 5, Final biLST < Encoder + Softmax Classifier
        Decoder = Bidirectional(LSTM(units=300, return_sequences=True),
                                name='finaldecoer')  # [-1,2*units]
        final_p = Dropout(self.DropProb)(Decoder(PremAlign))
        final_h = Dropout(self.DropProb)(Decoder(HypoAlign))

        AveragePooling = Lambda(lambda x: K.mean(x, axis=1))  # outs [-1, dim]
        MaxPooling = Lambda(lambda x: K.max(x, axis=1))  # outs [-1, dim]
        avg_p = AveragePooling(final_p)
        avg_h = AveragePooling(final_h)
        max_p = MaxPooling(final_p)
        max_h = MaxPooling(final_h)
        Final = keras.layers.Concatenate()([avg_p, max_p, avg_h, max_h])
        Final = Dropout(self.DropProb)(Final)
        Final = Dense(300,
                      kernel_regularizer=l2(self.L2Strength),
                      bias_regularizer=l2(self.L2Strength),
                      name='dense300_' + self.dataset,
                      activation='tanh')(Final)
        Final = Dropout(self.DropProb / 2)(Final)
        Final = Dense(3,
                      activation='softmax',
                      name='judge300_' + self.dataset)(Final)
        self.model = Model(inputs=[premise, hypothesis], outputs=Final)

    @time_count
    def compile_model(self):
        """ Load Possible Existing Weights and Compile the Model """
        self.model.compile(optimizer=self.Optimizer,
                          loss='categorical_crossentropy',
                           metrics=['accuracy'])
        self.model.summary()
        fn = self.rnn_type + '_' + self.dataset + '.check'
        if os.path.exists(fn):
            self.model.load_weights(fn, by_name=True)
            print('--------Load Weights Successful!--------')

    def start_train(self):
        print('jinal.............')
        print(self.train)
        """ Starts to Train the entire Model Based on set Parameters """
        # 1, Prep
        callback = [EarlyStopping(patience=self.Patience),
                    ReduceLROnPlateau(patience=5, verbose=1),
                    CSVLogger(filename=self.rnn_type + 'log.csv'),
                    ModelCheckpoint(self.rnn_type + '_' + self.dataset + '.check',
                                    save_best_only=True,
                                    save_weights_only=True)]

        # 2, Train
        print(self.train[0])
        self.model.fit(x=[self.train[0], self.train[1]],
                       y=self.train[2],
                       batch_size=self.BatchSize,
                       epochs=self.MaxEpoch,
                       validation_data=([self.test[0], self.test[1]], self.test[2]),
                       callbacks=callback)

        # 3, Evaluate
        self.model.load_weights(self.rnn_type + '_' + self.dataset + '.check')  # revert to the best model
        self.evaluate_on_test()

    def evaluate_on_test(self):
        loss, acc = self.model.evaluate([self.test[0], self.test[1]],
                                        self.test[2], batch_size=self.BatchSize)
        print("Test: loss = {:.5f}, acc = {:.3f}%".format(loss, acc))



    @staticmethod
    def plotHeatMap(df, psize=(8, 8), filename='Heatmap'):
        ax = sns.heatmap(df, vmax=.85, square=True, cbar=False, annot=True)
        plt.xticks(rotation=40), plt.yticks(rotation=360)
        fig = ax.get_figure()
        fig.set_size_inches(psize)
        fig.savefig(filename)
        plt.clf()





if __name__ == '__main__':

    md = AttentionAlignmentModel(annotation='EAM', dataset='snli')
    #  md.load_data()
    print('preparing data...')
    md.prep_data()
    print('data prepared...')
    print('preparing embedding...')
    md.prep_embd()
    print('embeddings prepared...')
    _test = False
    # md.create_model(test_mode = _test)a
    # md.create_standard_attention_model()
    print('preparing model...')
    md.create_enhanced_attention_model()
    print('model prepared...')
    print('compiling model...')
    md.compile_model()
    print('model compiled...')
    # md.label_test_file()
    print('trining.....')
    md.start_train()
    print('TRAINING COMPLETE!!!!!')
    print('evaluating...')
    md.evaluate_on_test()
    print('evaluation done....')



