import pandas as pd
import os
import _pickle as pickle
import json
import numpy as np




def loadSummData_pickle(path):
    # path = os.path.join(os.getcwd(),path)
    # outputpath = os.path.join(os.getcwd(), 'processed_data/bbc_dataset.pkl')
    # with open(path, 'r') as fp:
    #      dict = fp.read()
    # s = pickle.dumps(dict)
    # with open(outputpath,'wb')as f:
    #     f.write(s)
    print(os.getcwd())
    path = os.path.join(os.getcwd(),path)
    outputpath = os.path.join(os.getcwd(), 'processed_data/bbc_dataset.pkl')
    with open(path, 'rb') as fp:
         dict = fp.read()
    pickle.dump(dict, open(outputpath, "wb"))



# def loadSummData_json(path):
#     path = os.path.join(os.getcwd(), path)
#     outputpath = os.path.join(os.getcwd(), 'processed_data/save.json')
#     with open(path, 'r') as fp:
#         dict = fp.read()
#     s = json.dumps(dict)
#     with open(outputpath, "w") as f:
#         f.write(s)


def preprocess(path):
    #path = 'signalmedia-1m.jsonl/sample-1M.jsonl'

    #loadSummData_pickle(path)

    #loadSummData_json(path)

    df = pd.read_json(path, lines=True)
    return df

def process_glove(path):
    embeddings_index = {}
    f = open(os.path.join(path, 'glove.6B.50d.txt'),encoding='UTF-8',errors='ignore')
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()


    print('Found %s word vectors.' % len(embeddings_index))
    embedding_matrix = np.zeros((len(embeddings_index) + 1,256))
    # for word, i in word_index.items():
    #     embedding_vector = embeddings_index.get(word)
    #     if embedding_vector is not None:
    #         # words not found in embedding index will be all-zeros.
    #         embedding_matrix[i] = embedding_vector
    return embedding_matrix, embeddings_index
